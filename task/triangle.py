def is_triangle(a, b, c):
    '''
    Determine if a valid triangle with nonzero area can be constructed with the given side lengths.

    :param a: Length of the first side.
    :param b: Length of the second side.
    :param c: Length of the third side.

    :return: True if a triangle can be formed; False otherwise.
    '''

    x, y, z = sorted([a, b, c])
    return x > 0 and y > 0 and 0 < z < (x + y)


if __name__ == '__main__':
    tests = [
        (1, 2, 3),
        (2, 2, 3),
        (-1, 2, 4),
        (4, 3, 2),
        (5, 3, 3)
    ]
    for test in tests:
        print(test, is_triangle(*test))
